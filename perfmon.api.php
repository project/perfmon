<?php

/**
 * @file
 * Hooks provided by Performance Monitor module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Declare custom test which to be call on Performance Monitor tests running.
 *
 * Test callbacks must be return associative array. Possible keys are:
 * 'result': Test run result.
 * 'value': Nominal value for check.
 *
 * @return array
 *   An associative array where the key is the test`s machine_name and the
 *   value is again an associative array. Possible keys are:
 *   - 'title': test label.
 *   - 'callback': callback function name which calledd on test run.
 *   - 'description': test description, showed on result page.
 */
function hook_perfmon_performance_tests() {
  return array(
    'my_custom_test' => array(
      'title' => t('My custom test name'),
      'callback' => 'my_custom_test_run',
      'description' => t('Check the custom functionally on server.'),
    ),
  );
}

/**
 * @} End of "addtogroup hooks".
 */
